##    CS 101
##    Program 6 
##    Jackie Batson
##    jbvx8@umkc.edu
##    Created: 4/5/15-4/12/15
##    Due: 4/12/15
##
##    Problem: Given a viewer, recommend TV shows for that viewer to watch based on the highest
##             show ratings given by other viewers with similar tastes.
##
##    Algorithm:
##    1. Prompt the user for a csv file of user TV show ratings.  Check for valid files.
##       If invalid, display a warning and prompt again until a valid entry is made.
##    2. For each line in the file:
##            1) make the first line a header list
##            2) append a viewer list with each line after the first, to end up with a list of lists:
##               one list for each viewer and all their ratings.
##    3. Close the file
##    4. For each viewer in the user viewer:
##            1) For each score in viewer:
##                    a) append the first entry (user's name) to a new score list containing integer scores
##                    b) check if each of the rest of the entries are in a list of valid entries
##                    c) if they are not valid, append the score list with 0.
##                    d) if they are valid, append the score list with the integer value of the score
##            2) Append an integer viewer list with each score list
##    5. To get the average scores for all users for each show:
##            1)build a total_sum list starting with a slice of the first score list entry in the integer
##              list, from the second value on (eliminating the user's name)
##            2)for every other item in the integer viewer list:
##                    a) for each score in score list:
##                            i) if the index is not 0 (not the name), increment the total_sum list at
##                               index - 1 with the score
##            3)for each item in enumerated total_sum list:
##                    a)append a total_average list with total_sum[index]/length of the integer user list
##    6. Prompt the user to enter a name to find recommendations for.  Build a list of viewers from the first
##       value in each list in the integer user list.  If the entered user is not in that list, display a warning
##       and prompt for another user until a valid entry is made.
##    7. To get the scores for the chosen name: iterate through each viewer in the integer viewer list, if
##       viewer[0] equals name, pop that index from the integer viewer list, set it to a separate chosen viewer
##       list, and exit the loop.  The integer viewer list should now contain scores for everyone but the chosen
##       viewer and we should have a chosen viewer list of scores only for the chosen viwer.
##    8. To multiply the chosen viwer's scores by all of the other scores and add them, for each viwer in the
##       integer viwer list:
##            1)for each score in the enumerated viewer:
##                    a)if the index is 0 (i.e. name), append a new multiplied score list with score (i.e. name)
##                      and append a new viewer with score(i.e. name)
##                    b)else append multiplied score with score * chosen_viewer_list[index] and increment the viewer
##                      sum by same value.
##            2)append the viewer list sum with the viewer sum, and append a list of all viewer sums with each
##              viewer list sum. We will now have a list of just viewers and the sum of their ratings multiplied by
##              the chosen viewer's, e.g. viewer's_total_sum = [[Ken, -40],[Jon, 30]]
##    9. To get the top five similar viewers:
##            1)for each item in viewers_total_sum, reverse each item. Then sort viwers_total_sum and reverse it
##              (so largest numbers are first)
##            2)top_five_viewers = slice of first five entries in viewers_total_sum
##            3)for each item in top_five_viewers, reverse the item to get the name first again
##    10. To get the scores of the top five viewers, for each item in top_five_viewers:
##            1)for each viewer in integer viewer list:
##                    a)if the first value in item == the first value in viewer:
##                            i)append a new list top_five_scores with viewer
##            2)append the first index of item to a new list of top_five_names to get just the names of the top five.
##    11. Average the scores of the viewers in top_five_scores similar to step 5, building a new list top_five_average
##    12. To match the top five averages to the titles:
##            1)for each item in enumerated top_five_average, append a new title_average list with a list of
##              [item, total_average[index], header_list[index+1]].
##            2)sort then reverse the title_average list to order it according to highest top five average
##    13. Prompt the user to enter whether they'd like to see results for all shows or just shows the chosen user hasn't
##        seen.  Check that the entry is valid.  If not, display an error and prompt again.
##    14. If the user would like to see all shows:
##            1)for item in enumerated title_average:
##                    a) print(item[2], item[0], item[1]).  This will display the title of the show followed by the
##                       average for the top five similar viewers, followed by the average for all viewers.
##    15. If the user would like to see results for only the shows not seen:
##            1)for item in the enumerated chosen viewer list:
##                    a)if it is not the first item, append a list of chosen_viewer_titles with [header_list[index], item]
##            2)for item in chosen_viewer_titles:
##                    a)if item[1] == 0 (not seen), append a not_seen_list with item[0] (title)
##            3)for item in title_average:
##                    a)if item[2] in not_seen_list (i.e. if the title is in the list of not seen titles):
##                            i) print(item[2], item[0], item[1]).  This will display the title of the show followed by
##                               the average for the top five similar viewers, followed by the average for all viewers.
##    16. Prompt the user to enter whether they'd like to search for another viewer.  If yes, return to step 1.  If no, exit.
##
##    Error handling: Check for valid csv file entered, valid user name entered, valid menu choice entered for all titles or
##                    not seen titles, and valid menu choice entered at the try again prompt.



import csv

def get_file(prompt):
    """Prompts the user for a csv file to open. Checks if it is valid input and
       reprompts until a valid entry is made.
       :param prompt: String displayed to the user asking for a csv file to open.
       :return file_handle: the open file stream.
       """

    while True:
        try:
            file_entry = (input(prompt))
            if file_entry[-3:] == "csv": #small check to see if the user is entering a csv file
                file_handle = open(file_entry)
                return file_handle
            else:
                print("Not a csv file. Try again.") #will not return unless the entered file ends in "csv" and is in the directory.
        except FileNotFoundError:
            print("Could not find specified file. Try again.")
            

def read_csv_file():
    """Reads the csv file given by the user and returns a header list and a viewer
       list.
       :return header_list: a list of show titles
       :return viewer_list: a 2D list of lists, one list for each user, starting with
                the user's name, followed by their scores for shows in the same order as
                the header_list.
       """

    file_name = get_file("Enter the name of the csv file with ratings data: ")
    file_csv = csv.reader(file_name)
    header_list, viewer_list = [], []

    for lineno, line, in enumerate(file_csv):
        if lineno == 0:
            header_list = line  #builds a list of titles from the first line in the csv file
        else:
            viewer_list.append(line) #builds a 2D list with one list for each viewer and their ratings.

    file_name.close()
    return header_list, viewer_list


def get_integer_scores(user_list):
    """Takes a list of viewer scores and converts string scores into integers.
       :param user_list: the list of viewer scores read from the csv file, in string
       format.
       :return int_viewer_list: viewer list scores as integers instead of strings.

       This code also assigns 0 to invalid score entries, assuming the viewer hasn't seen
       the show.
       """

    valid_scores = ["-5", "-3", "0", "3", "5"]
    int_list = []
    
    for user in user_list:
        user_int = [] 
        for index, score in enumerate(user):
            if index == 0:
                user_int.append(score) #append the name
            else:
                if score not in valid_scores:   #appends 0 if the file contains an invalid score, assumes the viewer has not seen the show.
                    user_int.append(0)          
                else:                  
                    user_int.append(int(score)) #turns the string score into an integer.   
        int_list.append(user_int)   #append each list to a 2D integer list.
    return int_list


def get_average_list(number_list):
    """Gets the average of the values in a 2D list, returns a list of just the average
       values.
       :param number_list: a 2D list where each list contains numerical values to average
       over all the lists.
       :return average_list: a list of the averaged values.
       """

    sum_list, average_list = [], []
    for index, lst in enumerate(number_list):
        if index == 0:
            sum_list = lst[1:]  #copies only the scores in the first list to the sum list (excluding the name)
        else:
            for index, number in enumerate(lst):
                if index != 0:
                    sum_list[index - 1] = sum_list[index - 1] + number #increment each entry in the sum list by the entry at the index of the next list (-1 in the sum list because the sum list does not include names.
                                                                       
    for index, item in enumerate(sum_list):                            
        average_list.append(sum_list[index]/len(number_list))  #divide each value by the number of viewers to get the average.

    return average_list


def get_user(prompt, viewer_list):
    """Prompts the user to enter a viewer to find recommendations for.  If the viewer does
       not have ratings in the file, an error is displayed and the user is re-prompted for
       a name.
       :param prompt: String displayed to the user asking for a name.
       :param viewer_list: List of lists containing ratings for each viewer and their names.
       :return chosen_viewer_list: A list of just the chosen viewer and their ratings.

       This function will remove the chosen viewer from the full viewer list.
       """
    
    chosen_viewer_list = []

    while True:
        print()
        name = input(prompt)
        for index, viewer in enumerate(viewer_list):
            if viewer[0].upper() == name.upper():
                chosen_viewer_list = viewer_list.pop(index) #will modify the viewer list to exclude the chosen viewer.
                return chosen_viewer_list   
        print("Sorry, that viewer does not have ratings in the specified file. Try again.")


def match_viewer(viewer_list, chosen_viewer_list):
    """Multiplies the scores for each viewer by the scores for the chosen viewer.  Sums the
       multiplied scores in each list and appends these to a new 2D list containing the viewer
       names and their total of multiplied scores with the chosen viewer.
       :param viewer_list: A 2D list containing viewer names and ratings. Does not include the
       chosen viewer.
       :param chosen_viewer_list: A list containing only the scores for the chosen viewer.
       :return match_sum: A 2D list containing the names of each user and their total of scores
       multiplied by the chosen viewer's scores.

       The returned match_sum list will be ordered by highest matching scores.
       """

    match_sum = []

    for index, viewer in enumerate(viewer_list):
        viewer_sum_list = []
        viewer_sum, multiplied_score = 0, 0
        
        for index, score in enumerate(viewer):
            if index == 0:
                viewer_sum_list.append(score)  #append the name to the viewer sum list
            else:
                multiplied_score = score * chosen_viewer_list[index] #for all other indexes, 
                viewer_sum += multiplied_score                       #increment a viewer sum by the multiplied score.
                
        viewer_sum_list.append(viewer_sum) #append the viewer sum to the viewer sum list, so each sum list contains only the viewer name and summed multiplied score.
        match_sum.append(viewer_sum_list)  #append a 2D match list with each viewer sum list.


    for item in match_sum:
            item[0], item[1] = item[1], item[0] #swap the values so the multiplied scores are first, so they can be sorted by score.
    match_sum.sort() #sort the match viewer list by the multiplied score sum.
    match_sum.reverse() #reverse the list so the highest scores are first.

    return match_sum


def get_top_five_scores(match_sum, viewer_list):
    """Uses a ranked list of multiplied and summed ratings to get a list containing the ratings
       of only the top five most compatible viewers to the chosen viewer.
       :param match_sum: A 2D list containing the names of the viewers with their sum multiplied
       scores matched to the chosen viewer.
       :param viewer_list: A 2D list containing all the viewers excluding the chosen viewer and
       their ratings for all shows.
       :return top_five_scores: A 2D list containing only the top five viewers and all their
       original scores.
       """

    top_five_viewers = match_sum[:5]  #choose only the first 5 viewers in the sorted and reversed list.
    for item in top_five_viewers:
        item.reverse()  #get the names first again

    top_five_scores = []

    for item in top_five_viewers:
        for viewer in viewer_list:
            if item[0] == viewer[0]:  #if a name in the top five viewer list matches a name in the viewer list
                top_five_scores.append(viewer) #build a 2D list of each top 5 viewer and their scores.

    return top_five_scores


def get_top_five_names(top_five_scores):
    """Makes a list of just the names of the top five matching viewers.
       :param top_five_scores: A 2D list of the top five viewers and their scores.
       :return top_five_names: A list containing only the names of the top five matching viewers.
       """

    top_five_names = []
    for item in top_five_scores:
        top_five_names.append(item[0])

    return top_five_names


def get_title_averages(top_five_average, total_average, header_list):
    """Makes a 2D list containing each title and its average for all viewers and its average
       for the top five viewers.
       :param top_five_average: A list of average scores for the top five matched viewers.
       :param total_average: A list of average scores for all viewers (including chosen.
       :param header_list: A list of titles.
       :return title_averages: A 2D list containing each show's title, top five average, and
       total average.

       Will sort the averages according to the top five average scores, so those will come
       first in the lists.
       """

    title_averages = []
    for index, item in enumerate(top_five_average):
        title_averages.append([item, total_average[index], header_list[index+1]]) #builds a 2D list of titles and averages header_list is +1 to skip over "Name"
    title_averages.sort() #sort by top five averages                                                        
    title_averages.reverse() #put in decending order

    return title_averages


def get_user_choice(prompt, valid_choices):
    """Prompts the user for a choice and continues reprompting until a valid choice is entered.
       :param prompt: String displayed to the user asking to make a choice.
       :return user_choice: The user's selected entry.
       """
    
    while True:
        print()
        user_choice = input(prompt).upper()
        if user_choice not in valid_choices:
            print("Not a valid entry. Try again.") #Works for several prompts.
        else:
            return user_choice


def print_names(name, top_five_names):
    """Prints out the chosen viewer name and their top five match names. 
       :param chosen_name: the chosen user name
       :param top_five_names: List containing the names of the top five matched viewers.

       This function does not return a value, it just prints the chosen viewer name and top five match
       names.
       """

    print()
    print("Recommended shows for user", name)

    print("Your top matches are:", end = " ")
    for index, item in enumerate(top_five_names):
        if index < (len(top_five_names) - 1): #Formatting the sentence so it reads like "1, 2, 3, 4, and 5"
            print(item, end = ", ")
        else:
            print("and", item)


def print_results(title_averages, user_choice, chosen_viewer_list, header_list):
    """Prints out the resulting averages based on the user's choice of all shows or shows not seen.
       :param title_averages: 2D list containing each show title and its average ratings for all viewers
       and the top five matching viewers.
       :param user_choice: The user's entered choice for viewing all shows or shows not seen.
       :param chosen_viewer_list: List of the chosen viewer's ratings for all shows.
       :param header_list: List of show titles.

       This function does not return a value, it just prints the results based on the user's choice.
       """

    chosen_viewer_titles, not_seen_list = [], []

    print()
    print("{:35}{:>20}{:>20}".format("Show", "Top 5 Average", "Total Average"))
    print("=" * 80)
    if user_choice == "1":
        for index, item in enumerate(title_averages):
            print("{:35}{:20.4f}{:20.4f}".format(item[2], item[0], item[1])) #prints the list entries in correct order.
    else:
        for index, item in enumerate(chosen_viewer_list):
            if index != 0:
                chosen_viewer_titles.append([header_list[index], item]) #builds a 2D list of titles and the chosen viewer's score for that title.
        for item in chosen_viewer_titles:
            if item[1] == 0:
                not_seen_list.append(item[0]) #if the chosen viewer has not seen a certain title, add the title to a not seen list
        for index, item in enumerate(title_averages): 
            if item[2] in not_seen_list:  #if the title in title_averages is in the not seen list
                print("{:35}{:20.4f}{:20.4f}".format(item[2], item[0], item[1])) #print the title average items in correct order.



if __name__ == "__main__":

    csv_file = read_csv_file() 
    headers_list = csv_file[0]
    viewer_string_list = csv_file[1]

    viewer_int_list = get_integer_scores(viewer_string_list)
    total_average_list = get_average_list(viewer_int_list)

    try_again = "Y"
    while try_again in ["YES", "Y"]: #repeats the rest until the user does not want to try again.

        viewer_list_copy = viewer_int_list.copy() #need to make a copy because this list gets modified when the chosen user is popped.

        chosen_list = get_user("Enter the name of a viewer to find recommendations for: ", viewer_list_copy)
        chosen_name = chosen_list[0]

        summed_match_list = match_viewer(viewer_list_copy, chosen_list)
        top_five_ratings_list = get_top_five_scores(summed_match_list, viewer_list_copy)
        top_five_names_list = get_top_five_names(top_five_ratings_list)
        top_five_average_list = get_average_list(top_five_ratings_list)

        title_average_list = get_title_averages(top_five_average_list, total_average_list, headers_list)

        choice_prompt = """Would you like to view recommendations for all shows or just the shows not seen?
        \n1) All shows\n2) Only shows not seen\n=> """
        valid_menu_choices = ["1", "2"]

        choice = get_user_choice(choice_prompt, valid_menu_choices)
        print_names(chosen_name, top_five_names_list)
        print_results(title_average_list, choice, chosen_list, headers_list)

        valid_try_again = ["YES", "Y", "NO", "N"]
        try_again = get_user_choice("Would you like to find recommendations for another viewer? ", valid_try_again)
        print()
    print("Bob Dole thanks you for using Bob Dole's recommendation program.")
    
    
   





        



    
    
            












